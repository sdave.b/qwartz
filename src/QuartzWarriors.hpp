/* QuartzWarriors.hpp
 * Copyright © 2020 Saul D. Beniquez
 * License: This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef QUARTZWARRIORS_HPP_DHRBMJSV
#define QUARTZWARRIORS_HPP_DHRBMJSV

#include <stdexcept>
#include <memory>
#include <vector>

#include "framework/core/ComponentAllocator.hpp"
#include "framework/core/IGame.hpp"

// I'm getting ahead of myself again

#include "framework/model/PositionDataComponent.hpp"
#include "framework/model/ViewDataComponent.hpp"
#include "framework/model/ComponentPool.hpp"
#include "framework/view/RenderedCircle.hpp"
#include "framework/core/EventDispatcher.hpp"

#include "system/sdl.hpp"

namespace QW {
	class QuartzWarriors : public QW::IGame 
	{ 
	public:
		QuartzWarriors();

		virtual ~QuartzWarriors();

		virtual void Init(int argc, char* argv[]);

		virtual const bool& IsRunning() const;
		virtual int Run();

		virtual void Stop();
	protected:
		virtual void ProcessUserEvents();
		virtual void UpdateModel();
		virtual void Render();
	private:

//		using PositionDataAllocator = Core::ComponentAllocator<Model::PositionDataComponent>;
//		using PositionDataPool = std::vector<Model::PositionDataComponent, PositionDataAllocator>;
//
//		using RenderedCircleAllocator = Core::ComponentAllocator<View::RenderedCircle>;
//		using ViewDataPool     = std::vector<View::RenderedCircle, RenderedCircleAllocator>;

		using PositionDataPool = Model::ComponentPool<Model::PositionDataComponent>;
		using ViewDataPool = Model::ComponentPool<View::RenderedCircle>;

		bool isRunning;
		Uint32 ticks;
		const IComponent::entity_id playerId = 0;

		QW::Core::EventDispatcher eventDispatcher;

		PositionDataPool  positions;
		ViewDataPool 	  views;

		//QW::Model::PositionDataComponent playerPos;
		//QW::View::RenderedCircle playerAppearance;
		SDL::renderer_info_t renderer;
	};
}

#endif /* end of include guard: QUARTZWARRIORS_HPP_DHRBMJSV */
// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
