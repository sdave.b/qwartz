/* test-includes.hpp
 * Copyright © 2020 Saul D. Beniquez
 * License: This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef TEST_INCLUDES_HPP_GPYORIXZ
#define TEST_INCLUDES_HPP_GPYORIXZ

#include <catch.hpp>
#include <fakeit.hpp>

#include "framework.hpp"

using fakeit::Mock;
using fakeit::When;

#endif /* end of include guard: TEST_INCLUDES_HPP_GPYORIXZ */
// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8  noexpandtab ft=cpp.doxygen :
