/* test-frameworks.cpp
 * Copyright © 2018-2021 Saul D Beniquez
 * License: This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "test-includes.hpp"

class SomeInterface
{
public:
	virtual int foo(int) = 0;
	virtual int bar(c_string) = 0;
};


TEST_CASE("Catch2 works with Fakeit", "[integrity-check]")
{
	Mock<SomeInterface> mock_obj;
        Mock<SomeInterface> other_obj;

        // Use the preprocessor and template generator to create
	// two implementations of SomeInterface
	When(Method(mock_obj,foo)).Return(1); // returns 1 once
        When(Method(other_obj, foo)).Return(-1); //  returns 0 once

        // Get an instance of the generated mock class
	auto& iface = mock_obj.get();

	REQUIRE(iface.foo(0) == 1);

        auto &otheriface = other_obj.get();
        REQUIRE(otheriface.foo(0) == -1);
}
