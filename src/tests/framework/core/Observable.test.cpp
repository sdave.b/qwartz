/* Observable.test.cpp
 * Copyright © 2021 Saul D. Beniquez
 * License: This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <iostream>
#include <list>
#include <any>
#include <string>

#include "framework/core/INonAssignable.hpp"
#include "framework/core/IObserver.hpp"
#include "framework/core/Observable.hpp"

#include "test-includes.hpp"

using QW::Core::IObserver;
using QW::Core::Observable;

class ObservableViewer : protected QW::Core::Observable //#region
{
public:
	ObservableViewer(const Observable& other) : Observable(other) {}
	const size_t GetObserverCount() const
	{
		return observers.size();
	}
}; //#endregion
TEST_CASE("Register IObservers", "[class:Observable]") //#region
{
	Mock<IObserver> observerType1;
	Mock<IObserver> observerType2;
	Observable subject;

	IObserver& observer1 = observerType1.get();
	IObserver& observer2 = observerType2.get();

	subject.RegisterObserver(observer1);
	subject.RegisterObserver(observer2);

	const auto& subjectInspector = ObservableViewer(subject);

	REQUIRE(2 == subjectInspector.GetObserverCount());
} //#endregion
TEST_CASE("Can notify observers", "[class:Observable]") //#region
{
	Mock<IObserver> observerType1;
	Mock<IObserver> observerType2;
	Observable subject;

	static std::vector<std::string> buffer;

	When(Method(observerType1, OnNotice))
	  .Do([](const Observable& o, std::any m) {
		buffer.push_back("First Observer");
	}).AlwaysReturn();

	When(Method(observerType2, OnNotice))
	  .Do([](const Observable& o, std::any m) {
		buffer.push_back("Second Observer");
	}).AlwaysReturn();

	IObserver& observer1 = observerType1.get();
	IObserver& observer2 = observerType2.get();

	subject.RegisterObserver(observer1);
	subject.RegisterObserver(observer2);

	subject.Notify();

	REQUIRE(2 == buffer.size());
	CHECK(buffer.at(0) == "First Observer");
	CHECK(buffer.at(1) == "Second Observer");
}

// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8  noexpandtab ft=cpp.doxygen :
