/* debuginfo.h
 * Copyright © 2020 Saul D. Beniquez
 * License: This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef DEBUGINFO_H_ANPUL1TJ
#define DEBUGINFO_H_ANPUL1TJ

#include "config.h"
#include "framework/types.hpp"

#ifdef HAVE_EXECINFO_H
  #include <execinfo.h>
#else
 #include <boost/stacktrace.hpp>
#endif

#include <iostream>


inline void print_backtrace() {
#ifdef HAVE_EXECINFO_H
	 void* callstack[128];

	 int i, frames = backtrace(callstack, 128);
         char** strs = backtrace_symbols(callstack, frames);
         for (i = 0; i < frames; ++i) {
		 std::cout << strs[i] << std::endl;
         }
         free(strs);
#else
	std::cout << boost::stacktrace::stacktrace() << std::endl;
#endif

 }

inline void print_backtrace(std::exception& ex)
{
	std::cout << "caught: " << ex.what() << std::endl;
	print_backtrace();
}

inline void print_cmdline(int argc, const QW::c_string argv[]) {
	int i;

	std::cout << "Command-line received" << std::endl;
	for (i = 0; i < argc; ++i)
		std::cout << argv[i] << " ";
	std::cout << std::endl;
}

#endif /* end of include guard: DEBUGINFO_H_ANPUL1TJ */

// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
