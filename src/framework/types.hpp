/* qwop_Types.h
 * Copyright © 2018, Saul D Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef QWOP_TYPES_H_RAMNIAFT
#define QWOP_TYPES_H_RAMNIAFT

#include <cstdint>

#include "types/cstring.hpp"
#include "types/errors.hpp"

#endif /* end of include guard: QWOP_TYPES_H_RAMNIAFT */
