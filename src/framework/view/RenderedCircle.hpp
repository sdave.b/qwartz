/* RenderedCircle.hpp
 * Copyright © 2020 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


#ifndef RENDEREDCIRCLE_HPP_V1FE9FOP
#define RENDEREDCIRCLE_HPP_V1FE9FOP

#include "framework/core/IComponent.hpp"
#include "framework/model/ViewDataComponent.hpp"
#include "framework/model/PositionDataComponent.hpp"

namespace QW {
namespace View {

class RenderedCircle : public Model::ViewDataComponent
{
public:

	RenderedCircle(IComponent::entity_id id) :
		ViewDataComponent(id), entityID(id),
		radius(DEFAULT_RADIUS)
	{}

	//#region type information helper methods
	const_string GetName() const { return "RenderedCircle"; }
	const type_id  GetTypeId() const { return 0x1000; }
	const entity_id GetEntityId() const { return this->entityID; }
	//#endregion

	void Draw(SDL::renderer_info_t& rendererInfo,
		const Model::PositionDataComponent& position)
	{
		if (filledCircleRGBA(rendererInfo.renderer,
		              position.xPosition, position.yPosition, radius,
		              0x0,0xa,0xa0,0xff))  // color
		{
			throw std::runtime_error("Couldn't render a circle");
		}
	}

private:
	const int DEFAULT_RADIUS=10;
	IComponent::entity_id entityID;

	float radius;
};

}}

#endif /* end of include guard: RENDEREDCIRCLE_HPP_V1FE9FOP */
