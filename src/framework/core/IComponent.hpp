/* IComponent.hpp
 * Copyright © 2018 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef IENTITY_HPP_P40HKASO
#define IENTITY_HPP_P40HKASO

#include <memory>

#include "framework/types.hpp"
#include "framework/core/INonAssignable.hpp"

namespace QW {

	// #region Forward Declarations
	class IComponent;

	template<typename T>
	class IFactory;

	using IComponentFactory = IFactory<IComponent>;
	using IComponentFactory_ptr = std::shared_ptr<IComponentFactory>;
	// #endregion

	class IComponent : INonAssignable
	{
	public:
		typedef unsigned char entity_id;
		typedef unsigned short  type_id;

		virtual ~IComponent() {};

		virtual const_string     GetName() const = 0;
		virtual const   type_id  GetTypeId() const = 0;
		virtual const entity_id  GetEntityId() const = 0;

	protected:
		IComponent(entity_id id) : INonAssignable() {}

		friend class IFactory<IComponent>;
	};
}

#endif /* end of include guard: IENTITY_HPP_P40HKASO */
// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
