/* IObservable.hpp
 * Copyright © 2019 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef OBSERVABLE_HPP_STKAZCUF
#define OBSERVABLE_HPP_STKAZCUF

#include <list>
#include <functional>

namespace QW {
namespace Core {

	class IObserver;

	class Observable
	{
	public:
		void RegisterObserver(std::reference_wrapper<IObserver>);
		void Notify();
	protected:
		using observer_ref = std::reference_wrapper<IObserver>;
		using observer_list = std::list<observer_ref>;

		observer_list observers;
	};

}}

#endif /* end of include guard: IOBSERVABLE_HPP_STKAZCUF */
