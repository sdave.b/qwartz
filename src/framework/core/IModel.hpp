/* IModel.hpp
 * Copyright © 2018 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef IMODEL_HPP_CPJZNBGE
#define IMODEL_HPP_CPJZNBGE

#include "framework/core/INonAssignable.hpp"

namespace QW {
class IModel : INonAssignable
{
public:
	virtual ~IModel() {}
	virtual int HandleStep()  = 0;

protected:
	IModel(): INonAssignable() {}
};

}
#endif /* end of include guard: IMODEL_HPP_CPJZNBGE */
// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
