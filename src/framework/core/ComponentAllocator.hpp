/* ComponentAllocator.hpp
 * Copyright © 2020 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef COMPONENTALLOCATOR_HPP_LLX59OGJ
#define COMPONENTALLOCATOR_HPP_LLX59OGJ

#include <memory>
#include <cstdlib>

#include "framework/core/IComponent.hpp"

namespace QW {
namespace Core {

template <typename T>
class ComponentAllocator : public std::allocator<T>
{
// Guarantee:
// ComponentAllocator's template parameter <T> must be a descendant
// of IComponent
static_assert(std::is_base_of<IComponent, T>(),
      "Template type T must be derived from IComponent");

private:
	static IComponent::entity_id nextId;
public:

	T* allocate(size_t n, const T* = 0)
	{
	    if (T* buffer = static_cast<T*>(::operator new(n * sizeof(T)))) { 
		    return buffer;
	    }
	    throw std::bad_alloc();
	}

	T* deallocate(T* p, size_t n)
	{
		::operator delete(p);
		return p;
	}

	size_t max_size() const
	{
	    return std::numeric_limits<size_t>::max() / sizeof(T);
	}

	template <typename U, typename... Args>
	void construct(U* p, Args&&... args)
	{
	     new(p) U(nextId++);
	}

	template <typename U>
	void destroy(U* p)
	{
		p->~U();
	}

	template <typename U>
	struct rebind
	{
		using other = ComponentAllocator<U>;
	};
};

template <typename T>
IComponent::entity_id ComponentAllocator<T>::nextId = 0;
}}

#endif /* end of include guard: COMPONENTALLOCATOR_HPP_LLX59OGJ */
// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8  noexpandtab ft=cpp.doxygen :
