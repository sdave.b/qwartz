/* IGame.hpp
 * Copyright © 2018 Saul D Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef IGAME_HPP_E5U8I4CP
#define IGAME_HPP_E5U8I4CP

#include "framework/core/INonAssignable.hpp"
#include <any>

namespace QW {
class IGame : INonAssignable {
public:

	virtual ~IGame() {};

	virtual void Init(int argc, char* argv[]) = 0;

	virtual const bool& IsRunning() const = 0;
	virtual int Run() = 0;

	virtual void Stop() = 0;
protected:
	IGame() : INonAssignable() {};

	virtual void ProcessUserEvents() = 0;
	virtual void UpdateModel() = 0;
	virtual void Render() = 0;
};

}
#endif /* end of include guard: GAME_HPP_E5U8I4CP */

// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
