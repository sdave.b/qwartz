/* Observable.cpp
 * Copyright © 2020  Saul D Beniquez
 * License: MPL2
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef OBSERVABLE_CPP_HX8AZ9AG
#define OBSERVABLE_CPP_HX8AZ9AG

#include "Observable.hpp"
#include "framework/core/IObserver.hpp"

#include <functional>
#include "IObserver.hpp"

namespace QW {
namespace Core {

void Observable::RegisterObserver(std::reference_wrapper<IObserver> observer)
{
	this->observers.push_back(observer);
}

void Observable::Notify()
{
	for ( auto& observer  : this->observers)
	{
		observer.get().OnNotice(*this);
	}
}
}}
#endif /* end of include guard: OBSERVABLE_CPP_HX8AZ9AG */
// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
