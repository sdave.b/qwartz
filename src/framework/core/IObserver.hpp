/* IObservable.hpp
 * Copyright © 2020 Saul D. Beniquez
 * License:  MPL2
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef IOBSERVABLE_HPP_98KR62NA
#define IOBSERVABLE_HPP_98KR62NA

#include "INonAssignable.hpp"
#include <any>

namespace QW {
namespace Core {

	class Observable;

	class IObserver : INonAssignable
	{
	public:
		virtual void
		   OnNotice(const Observable& sender, std::any message = nullptr) = 0;
	};

}}
#endif /* end of include guard: IOBSERVABLE_HPP_98KR62NA *///
// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
