/* EventDispatcher.hpp
 * Copyright © 2020 Saul D.Beniquez
 * License: MPL2
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef EVENTDISPATCHER_HPP_ZAOHXUEU
#define EVENTDISPATCHER_HPP_ZAOHXUEU

#include "SDL.h"
#include "SDL_gamecontroller.h"
#include "SDL_hints.h"
#include "SDL_joystick.h"
#include "framework/core/Observable.hpp"
#include "framework/core/IObserver.hpp"

#include <vector>
#include <queue>
#include <any>
#include <functional>
#include <memory>
#include <iostream>
#include <mutex>

#include <SDL.h>

namespace {
	using SDL_JoystickDevice_ptr =
		std::unique_ptr<SDL_Joystick,decltype(&SDL_JoystickClose)>;
}

namespace QW {
namespace Core {

class EventDispatcher : public Observable
{
public:
	EventDispatcher(): joydev(nullptr,nullptr)
	{
	}

	~EventDispatcher()
	{
		if(joydev.get())
			joydev.reset();

		SDL_GameControllerEventState(SDL_DISABLE);

		SDL_QuitSubSystem(SDL_INIT_JOYSTICK);
		SDL_QuitSubSystem(SDL_INIT_GAMECONTROLLER);
		SDL_QuitSubSystem(SDL_INIT_EVENTS);
	}

	void InitJoysticks()
	{
		SDL_SetHint(SDL_HINT_JOYSTICK_ALLOW_BACKGROUND_EVENTS, "1");
		SDL_InitSubSystem(SDL_INIT_JOYSTICK);

		if (SDL_NumJoysticks() > 0) {
			joydev = SDL_JoystickDevice_ptr(SDL_JoystickOpen(0),
			                                  SDL_JoystickClose);
			std::cout << "Opened Joystick 0" << std::endl;
			std::cout << "Name: " << SDL_JoystickNameForIndex(0) << std::endl;
			std::cout << "Number of Axes: " << SDL_JoystickNumAxes(joydev.get()) << std::endl;
			std::cout << "Number of Buttons: " << SDL_JoystickNumButtons(joydev.get()) << std::endl;
			std::cout << "Number of Balls: " << SDL_JoystickNumBalls(joydev.get()) << std::endl;

			SDL_GameControllerEventState(SDL_ENABLE);
		} else {
			std::cout << "Warning: No Joystick or Controller "
			             "detected" << std::endl;
		}

	}

	void Enqueue(SDL_Event& e)
	{
		std::lock_guard<std::mutex> lock(eventQueueMutex);
		this->eventBuffer.push(e);
	}

	void RegisterObserver(std::reference_wrapper<IObserver> observer)
	{
		this->observers.push_back(observer);
	}

	void Notify()
	{
		while(!eventBuffer.empty())
		{
			auto& event = eventBuffer.front();

			for (auto& elem: observers)
			{
				elem.get().OnNotice(*this, event);
			}

                        std::lock_guard<std::mutex> lock(eventQueueMutex);
                        eventBuffer.pop();
		}
	}

private:
	std::vector<std::reference_wrapper<IObserver>> observers;
	std::queue<SDL_Event> eventBuffer;
	SDL_JoystickDevice_ptr joydev;

        std::mutex eventQueueMutex;
};

}}
#endif /* end of include guard: EVENTDISPATCHER_HPP_ZAOHXUEU */
// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
