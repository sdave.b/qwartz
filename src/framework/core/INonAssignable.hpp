/* NonAssignable.hpp
 * Copyright © 2018 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef NONASSIGNABLE_HPP_MZIRW73N
#define NONASSIGNABLE_HPP_MZIRW73N

namespace QW {

class INonAssignable {
protected:
	INonAssignable() = default;

	// Disable copy-constructor and assignment operator
	INonAssignable(INonAssignable const&) = delete;
	INonAssignable& operator=(INonAssignable const&) = delete;
};

}
#endif /* end of include guard: NONASSIGNABLE_HPP_MZIRW73N */
// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
