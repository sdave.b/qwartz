/* IFactory.hpp
 * Copyright © 2019 Saul D. Beniquez
 * License:  This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef IFACTORY_HPP_MYO3X50S
#define IFACTORY_HPP_MYO3X50S

#include <sys/types.h>
#include <cstdint>

#include "INonAssignable.hpp"

namespace QW {

template<typename T>
class IFactory : INonAssignable
{
public:
	virtual ~IFactory() {};

	virtual T& Get(const id_t id) const = 0;
protected:
	IFactory<T>() : INonAssignable() {}
};

}

// When the class is further developed, write the implementation up here
// #include "Ifactory.hpp.impl"

#endif /* end of include guard: IFACTORY_HPP_MYO3X50S */
// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
