/* strings.h
 * Copyright © 2019
 * License: This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef C_STRING_H_TLL76Y9K
#define C_STRING_H_TLL76Y9K

namespace QW {

// I *hate* using char* and char** types.
// Code should be easily readable
typedef char * c_string;
typedef const char* const_string;
typedef wchar_t * wc_string;

};

#endif /* end of include guard: STRINGS_H_TLL76Y9K */
