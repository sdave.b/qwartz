/* errors.hpp
 * Copyright © 2018 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef ERRORS_HPP_5TQSNWWJ
#define ERRORS_HPP_5TQSNWWJ

#include "cstring.hpp"

namespace QW {

typedef union error_code {
	unsigned int failure : 1;
	unsigned int code;
} error_code_t;

typedef struct error_info {
	error_code_t code;
	c_string msg;
} error_info_t;

}

#endif /* end of include guard: ERRORS_HPP_5TQSNWWJ */
// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
