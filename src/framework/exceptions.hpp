/* exceptions.hpp
 * Copyright © 2020 Saul D. Beniquez 
 * License: This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef EXCEPTIONS_HPP_H1JZ5V0G
#define EXCEPTIONS_HPP_H1JZ5V0G

#include <exception>
#include <stdexcept>

#define NOT_IMPLEMENTED std::runtime_error("Unimplemented Method");

#endif /* end of include guard: EXCEPTIONS_HPP_H1JZ5V0G */

// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
