/* ViewDataComponent.hpp
 * Copyright © 2019 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef VIEW_DATA_COMPONENT_HPP_RQVD0E2S
#define VIEW_DATA_COMPONENT_HPP_RQVD0E2S

#include "framework/core/IComponent.hpp"
#include "framework/model/PositionDataComponent.hpp"
#include "system/sdl.hpp"

#include <stdexcept>
#include <SDL2_gfxPrimitives.h>

namespace QW {
namespace Model {

/// \todo Rename to IViewDataComponent
class ViewDataComponent : public IComponent
{

public:
	virtual void Draw(SDL::renderer_info_t&,
	                  const PositionDataComponent&) = 0;
protected:
	ViewDataComponent(IComponent::entity_id id) : IComponent(id){};
};

}}
#endif /* end of include guard: VIEW_DATA_COMPONENT_HPP_RQVD0E2S */

// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
