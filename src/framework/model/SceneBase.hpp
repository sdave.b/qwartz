/* SceneBase.hpp
 * Copyright © 2019 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef SCENEBASE_HPP_BVXIWIAD
#define SCENEBASE_HPP_BVXIWIAD


#include <map>
#include <memory>

#include "framework/core/INonAssignable.hpp"
#include "framework/core/IFactory.hpp"

#include "framework/core/IComponent.hpp"


namespace QW {

class SceneBase : INonAssignable
{
public:
	virtual ~SceneBase() {}

	virtual std::shared_ptr<IComponentFactory>
		GetComponentFactory(IComponent::type_id type_id)
	{
		return factories[type_id];
	}

protected:
	SceneBase() : INonAssignable() {}
	std::map<IComponent::type_id, IComponentFactory_ptr> factories;
};

}
#endif /* end of include guard: ISCENE_HPP_BVXIWIAD */

// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
