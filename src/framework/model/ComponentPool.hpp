/* ComponentStore.hpp
 * Copyright © 2019 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef COMPONENTFACTORY_HPP_VRZ4DSTD
#define COMPONENTFACTORY_HPP_VRZ4DSTD

#include <type_traits>
#include <vector>

#include "framework/core/IFactory.hpp"
#include "framework/core/IComponent.hpp"
#include "framework/core/ComponentAllocator.hpp"


namespace QW {
namespace Model {

template <typename T>
class ComponentPool : public IFactory<T>
{
// Guarantee:
// Component Pool's template parameter <T> should be a descendant of IComponent
static_assert(std::is_base_of<IComponent, T>(),
	      "Template type T must be derived from IComponent");
public:
	ComponentPool();
	virtual ~ComponentPool();
	T& Get(const IComponent::entity_id id) const;

	// #region T.Get() overloads & aliases
	inline 	T& Get(const id_t id) const {
		auto entityId = static_cast<const IComponent::entity_id>(id);
		return this->Get(entityId);
	}
	inline T& operator[] (const id_t index) const  { return this->Get(index); }
	inline T& operator[] (const IComponent::entity_id index) const  { return this->Get(index); }
	// #endregion

private:
	Core::ComponentAllocator<T> allocator;
	std::vector<T,Core::ComponentAllocator<T> > componentVector;
};
}}

#ifndef VIM_COMPLETION
#include "ComponentPool.impl.hpp"
#endif

#endif /* end of include guard: COMPONENTFACTORY_HPP_VRZ4DSTD */

// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
