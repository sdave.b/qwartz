/* PositionComponent.hpp
 * Copyright © 2019 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef POSITIONCOMPONENT_HPP_RQVD0E2S
#define POSITIONCOMPONENT_HPP_RQVD0E2S

#include "SDL_events.h"
#include "SDL_keyboard.h"
#include "framework/core/IComponent.hpp"
#include "framework/core/IObserver.hpp"
#include "framework/core/Observable.hpp"

#include <any>
#include <iostream>
#include <queue>
#include <SDL.h>

#ifdef QW_DEBUG // #region JOYSTICK_DEBUG
#define JOYSTICK_DEBUG(Type, device)\
std::cout << Type << device.which << std::endl\
          << "Axis: " << static_cast<unsigned int>(device.axis) << std::endl\
	  << "value: " << device.value << std::endl;
#endif //#endregion

namespace QW {
namespace Model {

class PositionDataComponent : public IComponent, public Core::IObserver
{
public:
	float xPosition;
	float yPosition;
	float maxSpeed;

	PositionDataComponent(IComponent::entity_id id) :
	          IComponent(id), IObserver(),
	          xPosition(0.0), yPosition(0.0),maxSpeed(2.5)
	{}

	/* #region Component Type info methods */
	const_string GetName() const { return "PositionDataComponent"; }
	const type_id  GetTypeId() const { return 0x0000; }
	const entity_id GetEntityId() const { return 0x0000; }
	/* #endregion */

 	void OnNotice(const Core::Observable& sender, std::any message)
	{
		auto& sdlEvent = std::any_cast<SDL_Event&>(message);

		if (sdlEvent.type == SDL_JOYAXISMOTION ||
		    sdlEvent.type == SDL_KEYDOWN        ) {
			eventQueue.push(std::move(sdlEvent));
		}
	}

	/** @todo: replace this with ProcessCommand (command pattern equivalent)
	 ** Position Components shouldn't have to worry about SDL.
	 **/
	void ProcessEvents()
	{
		while(!eventQueue.empty()) {
			auto& sdlEvent = eventQueue.front();

			if (SDL_KEYDOWN == sdlEvent.type) {
				auto& scancode = sdlEvent.key.keysym.scancode;
				switch(scancode)
				{
				case SDL_SCANCODE_RIGHT:
					xPosition += maxSpeed;
					break;
				case SDL_SCANCODE_LEFT:
					xPosition -= maxSpeed;
					break;
				case SDL_SCANCODE_UP:
					yPosition -= maxSpeed;
					break;
				case SDL_SCANCODE_DOWN:
					yPosition += maxSpeed;
					break;
				default:
					break;
				}
			}
			else if (SDL_JOYAXISMOTION == sdlEvent.type) {
				auto& joydev = sdlEvent.jaxis;

				if (joydev.axis == 0) {
					if (joydev.value < -JOYSTICK_DEAD_ZONE) {
						xPosition -= maxSpeed;
					} else if (joydev.value > JOYSTICK_DEAD_ZONE) {
						xPosition += maxSpeed;
					}
				}
				if (joydev.axis == 1) {
					if (joydev.value < -JOYSTICK_DEAD_ZONE) {
						yPosition -= maxSpeed;
					} else if (joydev.value > JOYSTICK_DEAD_ZONE) {
						yPosition += maxSpeed;
					}
				}

#ifdef QW_DEBUG // #region
				if ((joydev.value > 0 && joydev.value > JOYSTICK_DEAD_ZONE) ||
				    (joydev.value < 0 && joydev.value < -JOYSTICK_DEAD_ZONE))
				{
					JOYSTICK_DEBUG("Joystick", joydev);
				}
#endif // QW_DEBUG #endregion
			}
			else if  (SDL_CONTROLLERAXISMOTION == sdlEvent.type) {
				auto& axis = sdlEvent.caxis;
#ifdef QW_DEBUG // #region
				JOYSTICK_DEBUG("Controller", axis);
#endif // QW_DEBUG #endregion
			}

			eventQueue.pop();
		}
	}
private:
	const int JOYSTICK_DEAD_ZONE = 8000;
	std::queue<SDL_Event> eventQueue;
};

}}
#endif /* end of include guard: POSITIONCOMPONENT_HPP_RQVD0E2S */

// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
