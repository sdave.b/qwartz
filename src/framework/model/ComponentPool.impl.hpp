/* ComponentPool.hpp.impl
 * Copyright © 2019 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef COMPONENTPOOL_HPP_IMPL_GA9GSUZH
#define COMPONENTPOOL_HPP_IMPL_GA9GSUZH

#include <vector>
#include <type_traits>

#include "framework/core/IFactory.hpp"
#include "framework/core/IComponent.hpp"

#define VIM_COMPLETION
#include "ComponentPool.hpp"
#undef VIM_COMPLETION


namespace {
	// Power of two function, evaluated at compiletime,
	// Used to allocate buffers
	template <class T>
	constexpr T pwrtwo(T exponent) {
	    return (T(1) << exponent);
	}
}

namespace QW {
namespace Model {

	const int COMPONENT_COUNT_MAX = pwrtwo<int>(sizeof(IComponent::entity_id));

	template <typename T>
	ComponentPool<T>::ComponentPool() : IFactory<T>(),
		 componentVector(COMPONENT_COUNT_MAX) {}

	template <typename T>
	ComponentPool<T>::~ComponentPool() {}

	template <typename T>
	T& ComponentPool<T>::Get(const IComponent::entity_id id) const
	{
		return const_cast<T&>(componentVector.at(id));
	}

}}

#endif /* end of include guard: COMPONENTPOOL_HPP_IMPL_GA9GSUZH */
// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
