/* Simple2DScene.hpp
 * Copyright © 2019 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef SIMPLE2DMODEL_HPP_F7CPQKI8
#define SIMPLE2DMODEL_HPP_F7CPQKI8

#include "framework/core/IModel.hpp"
#include "framework/model/SceneBase.hpp"

namespace QW {

class Simple2DScene : public IModel, public SceneBase
{
public:
	Simple2DScene() : IModel(), SceneBase() {}

	virtual int HandleStep()
	{
		return 0;
	}
};
}
#endif /* end of include guard: SIMPLE2DMODEL_HPP_F7CPQKI8 */

// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
