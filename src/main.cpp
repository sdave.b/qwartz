/* main.cpp
 * Copyright © 2018-2021 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#include "framework.hpp"
#include "QuartzWarriors.hpp"
#include "framework/debuginfo.hpp"

#include <list>
#include <iostream>
#include <memory>

int main(int argc, char* argv[])
{
	print_cmdline(argc, argv);


	//std::unique_ptr<IGame> game = QuartzWarriors::GetInstance();
	std::unique_ptr<IGame> game = std::make_unique<QuartzWarriors>();

	try
	{
		game->Init(argc, argv);
		return game->Run();
	}
	catch (std::exception& ex)
	{
		print_backtrace(ex);
	}

}

// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
