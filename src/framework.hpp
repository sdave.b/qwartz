/* frameworks.hpp
 * Copyright © 2018 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef FRAMEWORK_HPP_TDIZCEO3
#define FRAMEWORK_HPP_TDIZCEO3

#include "private/requirements.hpp"

#include "framework/debuginfo.hpp"
#include "framework/types.hpp"
#include "framework/exceptions.hpp"
#include "framework/core/IComponent.hpp"
#include "framework/core/IGame.hpp"

#include "system/sdl.hpp"

#endif /* end of include guard: FRAMEWORKS_HPP_TDIZCEO3 */
// vim: set foldmethod=marker foldmarker=@fold,@endfold textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
