/* QuartzWarriors.cpp
 * Copyright © 2020
 * License: This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <stdexcept>
#include <iostream>
#include <type_traits>
#include <thread>

#include "SDL.h"
#include "framework/core/IComponent.hpp"
#include "framework/exceptions.hpp"
#include "system/sdl.hpp"

#include "QuartzWarriors.hpp"

using namespace QW;


QuartzWarriors::QuartzWarriors() :
	isRunning(false), positions(), views(), IGame()
{
	this->renderer = SDL::new_renderer();
}

QuartzWarriors::~QuartzWarriors()
{
	close_renderer(&(this->renderer));
	SDL_QuitSubSystem(SDL_INIT_VIDEO);
	SDL_QuitSubSystem(SDL_INIT_TIMER);
};

void QuartzWarriors::Init(int argc, char* argv[])
{
	this->isRunning = true;

	this->eventDispatcher.RegisterObserver(positions.Get(playerId));


	SDL::initialize_renderer(this->renderer);
	SDL_InitSubSystem(SDL_INIT_TIMER);
	this->eventDispatcher.InitJoysticks();
}

const bool& QuartzWarriors::IsRunning() const
{
	return this->isRunning;
}

int QuartzWarriors::Run()
{
	// Define behavior of the background thread
	auto simulationLoop = [this]() -> void {
		do {
			this->eventDispatcher.Notify();
			this->UpdateModel();
			SDL_Delay(16);
		} while (this->IsRunning());
	};

        // Start the background thread.
        auto simulationThread = new std::thread(simulationLoop);

        // Begin processing things on this thread (main thread)
	do {
		ticks = SDL_GetTicks();
		this->ProcessUserEvents();

		//this->UpdateModel();
		this->Render();
	} while(this->isRunning);

	// Wait until the simulationLoop lambda finishes running
	// before proceeding to finish this thread.
        simulationThread.join();
        return 0;
}

void QuartzWarriors::Stop()
{
	this->isRunning = false;
}

void QuartzWarriors::ProcessUserEvents()
{
	SDL_Event e;
	SDL::input_status inputStatus;

	while(SDL::NO_EVENTS != (inputStatus = SDL::get_input(&e))) {

		if (SDL::ABORT == inputStatus) {
			this->Stop();
			break;
		}
		this->eventDispatcher.Enqueue(e);
	}

}

void QuartzWarriors::UpdateModel()
{
	this->positions.Get(playerId).ProcessEvents();
}

void QuartzWarriors::Render()
{
	Sint32 timeTaken;
	SDL::clear(this->renderer);

	using entity_id = IComponent::entity_id;

	// Render All the entities
	/// @todo: find a way to skip elements that have yet to get an ID
	/// @todo: find a way to filter only the elements in camera view
	for (entity_id i = 0 ; i < 1; ++i)
	{
		auto& position = this->positions[i];
		auto& view = this->views[i];

		view.Draw(this->renderer, position);
	}
	SDL::render(this->renderer);

	if ((timeTaken = SDL_GetTicks() - ticks) < 16) {

#ifdef QW_DEBUG //#region print framerate
		std::cout << (1000.0 / (16.0 - timeTaken)) << std::endl;
#endif // #endregion
		SDL_Delay(16 - timeTaken);
	}
}

//
// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
