/* requirements.h
 * Copyright © 2019 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef REQUIREMENTS_H_RQNNVY4Y
#define REQUIREMENTS_H_RQNNVY4Y

#include <config.h>

#if (!defined(HAVE_CXX17) | HAVE_CXX17 != 1)
	#error This framework requires C++17, at least
#endif

#endif /* end of include guard: REQUIREMENTS_H_RQNNVY4Y */
