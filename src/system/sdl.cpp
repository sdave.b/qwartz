/* frameworks-sdl.cpp
 * Copyright © 2018 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */

#ifndef FRAMEWORKS_SDL_CPP_GESAIUVW
#define FRAMEWORKS_SDL_CPP_GESAIUVW
#include "framework/types.hpp"
#include "system/sdl.hpp"


using namespace QW::SDL;

SDL::renderer_info_t SDL::new_renderer() {
	renderer_info_t retval = {
		false,  //error
		nullptr, //window
		nullptr, //screenSurface
		nullptr, //renderer
		nullptr, //texture
	};

	return retval;
}

/// @todo:  switch from printf to std::cout
int SDL::initialize_renderer(renderer_info_t& rendererInfo)
{
	SDL_SetHint(SDL_HINT_JOYSTICK_ALLOW_BACKGROUND_EVENTS,"1");
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL Could not initialize; SDL_Error: %s\n", SDL_GetError() );
		return -1;
	}
	IMG_Init(IMG_INIT_JPG);

	rendererInfo.window = SDL_CreateWindow("SDL Tutorial", 0,0,
	                                          1024,
	                                          768, SDL_WINDOW_SHOWN );
	if (rendererInfo.window == nullptr)
	{
		printf("Window could not be created! SDL_Error: %s\n",
				SDL_GetError());
		rendererInfo.error.code = 0x02;
		return (rendererInfo.error.code & 0x1000);
	}

	rendererInfo.renderer =
	      SDL_CreateRenderer(rendererInfo.window, 0,
	                         SDL_RENDERER_ACCELERATED);
	return 0;
}

void SDL::clear(renderer_info_t & rendererInfo)
{

	// Set bg to black
	SDL_SetRenderDrawColor( rendererInfo.renderer, 0, 0, 0, 0 );
	SDL_RenderClear(rendererInfo.renderer);
}

void SDL::render(renderer_info_t const & rendererInfo)
{
	SDL_RenderPresent(rendererInfo.renderer);
}


void SDL::close_renderer(renderer_info_t const * rendererInfo)
{
	if (rendererInfo->screenSurface) {
		SDL_FreeSurface(rendererInfo->screenSurface);
	}
	if (rendererInfo->window) {
		SDL_DestroyWindow(rendererInfo->window);
	}
}

input_status SDL::get_input(SDL_Event* eventPtr)
{
	if ((eventPtr != nullptr) && (SDL_PollEvent(eventPtr) != 0)) {
		if (eventPtr->type == SDL_QUIT) {
			return SDL::ABORT;
		}

		return SDL::MORE_EVENTS;
	}

	return SDL::NO_EVENTS;
}


#endif /* end of include guard: FRAMEWORKS_SDL_CPP_GESAIUVW */
// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
