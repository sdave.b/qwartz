/* frameworks-sdl.hpp
 * Copyright © 2018 Saul D. Beniquez
 * License: Mozilla Public License v. 2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v.2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at https://mozilla.org/MPL/2.0/.
 */
#ifndef FRAMEWORKS_SDL_HPP_TG49SYZX
#define FRAMEWORKS_SDL_HPP_TG49SYZX

#include <SDL.h>
#include <SDL_image.h>

#include "framework/types/errors.hpp"

using namespace QW;

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

namespace QW {
namespace SDL
{
	/// \todo Move to sdl_types.hpp
	typedef struct renderer_info_t {
		error_code_t error;

		SDL_Window*   window;
		SDL_Surface*  screenSurface;
		SDL_Renderer* renderer;
		SDL_Texture*  texture;

	} renderer_info;

	typedef enum input_status_t {
		NO_EVENTS = 0,
		MORE_EVENTS,
		ABORT
	} input_status;

	renderer_info new_renderer();

	// #region Rendering functions
	int initialize_renderer(renderer_info_t&);
	void clear(renderer_info_t &);
	void render(renderer_info_t const &);
	void close_renderer(renderer_info_t const *);
	// #endregion
	input_status get_input(SDL_Event* e);
}}
#endif /* end of include guard: FRAMEWORKS_SDL_HPP_TG49SYZX */
// vim: set foldmethod=marker foldmarker=#region,#endregion textwidth=80 ts=8 sts=0 sw=8 noexpandtab ft=cpp.doxygen :
