#!/bin/sh

die()
{
	rc=${2:-255}
	echo "Error: ${1}"

	exit ${rc}
}

verbose_exec()
{
	need_to_die=1

	if [ "${1}" == "--optional" ] || [ "${1}" == "-o" ]; then
		echo "optional"
		need_to_die=0
		shift;
	fi


	echo "$ ${@}"
	if [ ${need_to_die} -ne 0 ]; then
		${@} || die "${1} returned error code ${?}"
	else
		${@}
	fi

	#return ${?}
}

verbose_exec git config -f .gitmodules submodule.third_party/Catch2.shallow true
verbose_exec git config -f .gitmodules submodule.third_party/FakeIt.shallow true

verbose_exec -o git submodule update --init --recursive


if [ ${?} -ne 0 ]; then
	verbose_exec rm -rf third_party/Catch2
	verbose_exec rm -rf third_party/FakeIt

	verbose_exec git submodule update --init --recursive
fi

verbose_exec autoreconf -iv
if [ ! -x ./configure ]; then
	die "./configure does not exist. Check configure.ac and the autotools!"
fi
