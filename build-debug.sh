#!/bin/sh

BASE_DIR="$(cd "$(dirname "$0")"; pwd)";

mkdir -pv $BASE_DIR/debug
cd  $BASE_DIR/debug

source $BASE_DIR/debug.env

../configure

nice -n 15 make -j 2
